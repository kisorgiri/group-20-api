const mongoose = require('mongoose');
const dbConfig = require('./configs/db.config');


mongoose.connect(dbConfig.dbUrl + '/' + dbConfig.dbName,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }, function (err, done) {
        if (err) {
            console.log('error ', err);
        }
        else {
            console.log('db connection success');
        }
    });
