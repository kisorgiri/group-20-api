const express = require('express');
const router = express.Router();
const UserModel = require('./../models/user.model');
const map_user_req = require('./../helpers/map_user.req');
const passwordHash = require('password-hash');
const jwt = require('jsonwebtoken');
const config = require('./../configs');

const sender = require('./../configs/nodemailer.config');
const mailTemplate = require('./../templates/email_template');

function createToken(data) {
    var token = jwt.sign({
        id: data._id
    }, config.JWT_secret);
    // {
    //     expiresIn:'60'
    // }
    return token;
}

router.post('/login', function (req, res, next) {

    UserModel.findOne({
        $or: [
            {
                username: req.body.username
            },
            {
                email: req.body.username
            }
        ]
    })
        .then(function (user) {
            if (user) {
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {
                    res.json({
                        user,
                        token: createToken(user),
                    })
                } else {
                    next({
                        msg: "Invalid password"
                    })
                }
            } else {
                next({
                    msg: 'Invalid Username'
                })
            }
        })
        .catch(function (err) {
            next(err);
        })

});


router.post('/register', function (req, res, next) {
    console.log('req.body here >>', req.body);
    var newUser = new UserModel({});
    var newMappedUser = map_user_req(newUser, req.body);
    console.log('new user >>>', newMappedUser);
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200).json(done);
    })
});


router.post('/forgot-password', function (req, res, next) {
    console.log('req.body <>>>', req.body);
    // $in
    //  $all ==

    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                next({
                    msg: "No any account registered with provided email",
                    status: 404
                })
            }
            // http headers origin
            const data = {
                name: user.username,
                email: user.email,
                link: req.headers.origin + '/reset-password/' + user._id
            }
            const mailData = mailTemplate.auth.forgot_password(data);
            user.passwordResetTokenExpiry = Date.now() + 1000 * 60 * 60 * 24 * 2;
            // user.password = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                sender.sendMail(mailData, function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                });
            });

        })
});

router.post('/reset-password/:token', function (req, res, next) {
    UserModel.findOne({
        _id: req.params.token
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "user not found"
                });
            }
            if (user.passwordResetTokenExpiry < Date.now()) {
                return next({
                    msg: 'Password reset token expired'
                })
            }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetTokenExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(done);
            })
        })
})

module.exports = router;


//connect to db
// select db
// perfrom query
//disconnect