
const router = require('express').Router();

const UserModel = require('./../models/user.model');
const map_user_req = require('./../helpers/map_user.req');


router.route('/')
    .get(function (req, res, next) {
        // find all
        var condition = {};
        UserModel
            .find(condition, {
                password: 0,
            })
            .sort({
                _id: -1
            })
            // .limit(2)
            // .skip(2)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.json(users);
            })
    })
    .post(function (req, res, next) {

    });


router.route('/dashboard')
    .get(function (req, res, next) {

    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    })
    .post(function (req, res, next) {
        // res.redirect('/')  way of redirect
        res.json(req.body);
    });

router.route('/:id')
    .get(function (req, res, next) {
        // find by id
        // UserModel
        //     .findById(req.params.id)
        //     .then(function (data) {
        //         res.json(data);
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     })
        UserModel
            .findOne({ _id: req.params.id })
            .then(function (data) {
                res.json(data);
            })
            .catch(function (err) {
                next(err);
            })


    })
    .put(function (req, res, next) {
        // update
        UserModel.findById(req.params.id)
            .then(function (user) {
                if (user) {
                    var updatedMapUser = map_user_req(user, req.body);
                    user.save(function (err, saved) {
                        if (err) {
                            return next(err);
                        }
                        res.json(saved);
                    })
                }
                else {
                    next({
                        msg: "user not found"
                    })
                }
            })
            .catch(function (err) {
                next(err);
            })

    })
    .delete(function (req, res, next) {
        if (req.loggedInUser.role !== 1) {
            return next({
                msg: 'You dont have access'
            });
        }

        UserModel.findById(req.params.id)
            .then(function (user) {
                if (user) {
                    user.remove(function (err, done) {
                        if (err) {
                            next(err);
                        }
                        res.json(done);
                    })
                }
                else {
                    next({
                        msg: "user not found"
                    })
                }
            })
            .catch(function (err) {
                next(err);
            })
    });

module.exports = router;



