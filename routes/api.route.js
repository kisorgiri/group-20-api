const router = require('express').Router();
const AuthRoute = require('./../controllers/auth.route');
const UserRoute = require('./../controllers/user.route');
const ProductRoute = require('./../modules/products/routes/product.route');
const authentication = require('./../middlewares/authentication');
router.use('/auth', AuthRoute);
router.use('/user', authentication, UserRoute);
router.use('/product', ProductRoute);

module.exports = router;