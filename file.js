const fs = require('fs');

function write(fileName, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile(fileName, content, function (err, done) {
            if (err) {
                reject(err);
            }
            else {
                resolve(done);
            }
        })
    })


}

function read(fileName, cb) {
    fs.readFile(fileName, 'UTF-8', function (err, done) {
        if (err) {
            console.log('error >>', err);
            cb(err);
        }
        else {
            cb(null, done)
        }
    })
}


module.exports = {
    write,
    read
}