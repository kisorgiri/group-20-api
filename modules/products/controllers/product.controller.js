const ProductQuery = require('./../query/product.query');
const fs = require('fs');
const path = require('path');

function add(req, res, next) {
    // user to be added
    // image to be added
    // console.log('req.body >>', req.body);
    // console.log('req.file', req.file);
    if (req.fileErr) {
        return next({
            msg: "Invlaid file format",
            status: 400
        })
    }
    if (req.file) {
        var mimeType = req.file.mimetype.split('/')[0];
        if (mimeType !== 'image') {
            fs.unlink(path.join(process.cwd(), 'uploads/images/' + req.file.filename), function (err, done) {
                if (err) {
                    console.log('removing failed');
                } else {
                    console.log('file removed');
                }
            })
            return next({
                msg: "Invlaid file format",
                status: 400
            })
        }
    }
    var data = req.body;
    data.user = req.loggedInUser._id;
    if (req.file) {
        data.image = req.file.filename;
    }

    ProductQuery
        .insert(data)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function fetch(req, res, next) {
    var condition = {};
    if (req.loggedInUser.role != 1) {
        condition.user = req.loggedInUser._id;
    }
    var searchOptions = {}
    searchOptions.pageSize = req.query.pageSize;
    searchOptions.pageNumber = req.query.pageNumber;
    ProductQuery
        .find(condition, searchOptions)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function fetchById(req, res, next) {
    var condition = {
        _id: req.params.id
    }
    ProductQuery
        .find(condition)
        .then(function (data) {
            res.status(200).json(data[0]);
        })
        .catch(function (err) {
            next(err);
        });
}

function search(req, res, next) {
    console.log('req.body >>', req.body);
    const searchCondition = {};
    var mappedSearchCondition = ProductQuery.map_product_data(searchCondition, req.body);
    var searchOptions = {}
    searchOptions.pageSize = req.query.pageSize;
    searchOptions.pageNumber = req.query.pageNumber;
    if (req.body.minPrice) {
        mappedSearchCondition.price = {
            $gte: req.body.minPrice
        }
    }
    if (req.body.maxPrice) {
        mappedSearchCondition.price = {
            $lte: req.body.maxPrice
        }
    }
    if (req.body.minPrice && req.body.maxPrice) {
        mappedSearchCondition.price = {
            $gte: req.body.minPrice,
            $lte: req.body.maxPrice
        }
    }
    if (req.body.tags) {
        mappedSearchCondition.tags = {
            $in: [req.body.tags]
        }
    }
    console.log('map serch condition', mappedSearchCondition);
    ProductQuery
        .find(mappedSearchCondition, searchOptions)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });
}

function update(req, res, next) {
    var id = req.params.id;
    const data = req.body;
    if (req.file) {
        data.image = req.file.filename
    }

    ProductQuery
        .update(id, data)
        .then(function (data) {
            res.status(200).json(data.product);
            if (req.file && data.oldImage) {
                fs.unlink(path.join(process.cwd(), 'uploads/images/' + data.oldImage), function (err, done) {
                    if (err) {
                        console.log('removing failed');
                    } else {
                        console.log('file removed');
                    }
                })
            }


        })
        .catch(function (err) {
            next(err);
        })

}

function remove(req, res, next) {
    ProductQuery.remove(req.params.id)
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        })
}

module.exports = {
    add,
    fetch,
    update,
    remove,
    fetchById,
    search
}