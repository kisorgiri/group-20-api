const ProductModel = require('./../models/product.model');

function map_product_data(product, productDetails) {
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.category)
        product.category = productDetails.category;
    if (productDetails.brand)
        product.brand = productDetails.brand;
    if (productDetails.description)
        product.description = productDetails.description;
    if (productDetails.image)
        product.image = productDetails.image;
    if (productDetails.user)
        product.user = productDetails.user;
    if (productDetails.price)
        product.price = productDetails.price;
    if (productDetails.color)
        product.color = productDetails.color;
    if (productDetails.manuDate)
        product.manuDate = productDetails.manuDate;
    if (productDetails.expiryDate)
        product.expiryDate = productDetails.expiryDate;
    if (productDetails.name)
        product.name = productDetails.name;
    if (productDetails.tags) {
        if (typeof (productDetails.tags) === 'string') {
            product.tags = productDetails.tags.split(',');
        } else {
            product.tags = productDetails.tags;
        }
    }

    if (productDetails.discountedItem) {
        product.discount = {
            discountedItem: productDetails.discountedItem,
            discountType: productDetails.discountType,
            discount: productDetails.discount,
        }
    }
    if (productDetails.reviewPoint || productDetails.reviewMessage) {
        product.reviews.push({
            point: productDetails.reviewPoint,
            message: productDetails.reviewMessage
        })
    }
    return product;
}

/**
 * insert data
 * @param {object} data 
 * @returns promise
 */
function insert(data) {
    // return new Promise(function (resolve, reject) {
    //     var newProduct = new ProductModel({});
    //     var newMappedProduct = map_product_data(newProduct, data);
    //     newMappedProduct.save(function (err, done) {
    //         if (err) {
    //             reject(err);
    //         }
    //         resolve(done);
    //     })
    // })
    var newProduct = new ProductModel({});
    var newMappedProduct = map_product_data(newProduct, data);
    return newMappedProduct.save();


}

function find(condition, options = {}) {
    console.log('condition >>', condition);
    var perPage = Number(options.pageSize) || 100;
    var pageNum = (Number(options.pageNumber) || 1) - 1;
    var limitProduct = perPage * pageNum;
    return ProductModel
        .find(condition)
        .limit(perPage)
        .skip(limitProduct)
        .sort({
            _id: -1
        })
        .populate('user', {
            username: 1
        })

}
function update(id, data) {
    return new Promise(function (resolve, reject) {
        ProductModel.findById(id)
            .then(function (product) {
                if (product) {
                    var oldImage = product.image;
                    var updatedMappedProduct = map_product_data(product, data);
                    updatedMappedProduct.save(function (err, done) {
                        if (err) {
                            return reject(err);
                        }
                        resolve({
                            product: done,
                            oldImage
                        });
                    })
                } else {
                    reject({
                        msg: "Product Not Found",
                        status: 404
                    })
                }
            })
            .catch(function (err) {
                reject(err);
            })
    })


}
function remove(id) {
    return ProductModel.findByIdAndRemove(id);

}

module.exports = {
    insert,
    find,
    update,
    remove,
    map_product_data
}