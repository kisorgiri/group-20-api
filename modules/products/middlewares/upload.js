const multer = require('multer');
// simple ususage
// var upload = multer({ dest: './uploads/' })
var myStorage = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    },
    destination: function (req, file, cb) {
        cb(null, './uploads/images');
    }
})
function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType !== 'image') {
        req.fileErr = true;
        cb(null, false)
    } else {
        cb(null, true)
    }
}
var upload = multer({
    storage: myStorage,
    fileFilter: filter
});

module.exports = upload;