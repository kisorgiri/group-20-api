const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    message: String,
    point: Number
}, {
    timestamps: true
})

const ProductSchema = new Schema({
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    brand: {
        type: String,
    },
    description: String,
    category: {
        type: String,
        required: true
    },
    price: Number,
    color: String,
    image: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    tags: [String],
    reviews: [ReviewSchema],
    discount: {
        discountedItem: Boolean,
        discountType: String, // eg percentage, value, unit
        discount: String  // 10%, 200, 2
    },
    manuDate: Date,
    expiryDate: Date

}, {
    timestamps: true
})

const productModel = mongoose.model('product', ProductSchema);

module.exports = productModel;