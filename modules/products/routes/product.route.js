const ProductCTRL = require('./../controllers/product.controller');
const uploader = require('./../middlewares/upload');
const router = require('express').Router();
const authentication = require('./../../../middlewares/authentication');

router.route('/')
    .get(authentication, ProductCTRL.fetch)
    .post(authentication, uploader.single('img'), ProductCTRL.add)

router.route('/search')
    .get(ProductCTRL.search)
    .post(ProductCTRL.search)

router.route('/:id')
    .get(authentication, ProductCTRL.fetchById)
    .put(authentication, uploader.single('img'), ProductCTRL.update)
    .delete(authentication, ProductCTRL.remove)



module.exports = router;