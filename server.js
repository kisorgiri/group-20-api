const express = require('express');
const morgan = require('morgan');
const path = require('path');
const app = express();
const config = require('./configs');
const cors = require('cors');
//db connection
require('./db');

var event = require('events');
var errEvent = new event.EventEmitter();
app.use(function (req, res, next) {
    req.myEvent = errEvent;
    next();
})

errEvent.on('err', function (error, res) {
    console.log('error event here >>', error);
    res.json(error)

})

// socket stuff
var socket = require('socket.io');
var io = socket(app.listen(8081));
io.on('connection', function (client) {
    console.log('client connected to socket server');
    // client.emit
    client.on('from-fe', function (data) {
        console.log('message from fe', data);
        client.emit('from-be', 'hi from be');
    })
    client.on('new-msg', function (data) {
        console.log('new message from clinet', data);
        client.emit('reply-msg', data);
        client.broadcast.emit('reply-msg', data);
        // emit mesage to receiver
        // emit only send message to connected client
        // brodcast  will send message to every connected client
        // brodcast.to(val) will send to one specific client (private)
    })
});

// view engine setup
const pug = require('pug');
app.set('view-engine', pug)
app.set('views', path.join(__dirname, 'views'));

// load routing level middleware

const APIRoute = require('./routes/api.route');

// load middleware
const checkToken = require('./middlewares/checkToken');

// third party middleware
app.use(morgan('dev'));
app.use(cors());

// inbuilt middleware
app.use(express.static('files')); // internal ususage
app.use('/file', express.static(path.join(__dirname, 'uploads/images')));
// parse incoming data
app.use(express.urlencoded({
    extended: true,
}));
app.use(express.json());

app.use('/api', APIRoute);

// application level middleware which works as 404 error handler
app.use(function (req, res, next) {
    next({
        msg: "Not Found",
        status: 404
    })
});
app.use(function (err, req, res, next) {
    //  err is error block
    console.log('err is >>>', err);
    console.log('i am error handling middleware');
    // TODO set status code before sending response
    res
        .status(err.status || 400)
        .json({
            text: 'from error handling middleware',
            status: err.status || 400,
            msg: err.msg || err,
        });
});


app.listen(config.port, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log(`server listening at port ${config.port}`);
    }
});

// express and middleware are synonyms

// middlewares are function that has access to
// http request object
// http response object
// and next middleware function refrence
// it can access/modify http request and response
// middlewares always came into actions between request response cycle

// the order of middleware matters
// syntax
// function(req,res,next){
// req or 1st argument is http request object
// res or 2nd argument is http response object
// next or 3rd argument is next middleware function refrence
// }
// config block
// app.use(middlewarefn)


// types of middleware
// 1 application level middleware
// jata jata req and res ko scope vetincha those middleware are application level middleware
// 2 routing level middleware
// 3 third party middleware ==> every middleware that exists in npmjs.com
// 4 inbuilt middleware ==> epxress owned middleware
// 5 error handling middleware ===> catch block for every errors within application