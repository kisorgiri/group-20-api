// to access mongo shell
// mongo  ==> mongodb must be installed in your system and it must be running
// an arrow head interface will be avilable once connected
// shell command
// show dbs  list all available database
// use <db_name> if(db_name exist ){select existing database} else {create new database and select it}
// db show selected database
// show collection  list all the available collections
// insert document in collection
// db.<col_name>.insert({valid_json})

// find 
// db.<col_name>.find() // find all
// db.<col_name>.find({query builder}) // find all
// db.<col_name>.find({query builder}).count() // 
// db.<col_name>.find({query builder}).pretty() // prettify content (format)


// update
// db.<col_name>.update({},{},{});
// 1st object ===> query builder
// 2nd object  $set :{new data to be updated}
// 3rd object is optional , and used to provide options

// eg . db.users.update({_id:ObjectId(hex-code)},{$set:{name:'value '}},{upsert:true})

// remove
// db.<col_name>.remove({query_builder})

// drop collection
// db.<col_name>.drop();

// drop database
// db.dropDatabase()


// BACKUP & RESTORE

// mongodump mongorestore mongoimport and mongoexport
// two way==> machine redable format / human readable format

// 1 bson
// backup 
// mongodump ==> it will backup all the database in default dump folder
// mongodump --db <db_name>  it will back up selected database only
// mongodump -- db<db_name> --out <path to destination folder>

// restore 
// mongorestore ==> it will look for dump folder and restore all the database from dump
// mongorestoer <path_to_destination_folder> to restore from other then dump folder


// json and csv
// mongoexport and mongoimport (json)
// backup
// command
// mongoexport --db <db_name> --collection <col_name> --out <path to destinateion with .json extention>
// mongoexport -d <db_name> -c <col_name> -o <path to destinateion with .json extention>

// restore
// mongoimport --db<db_name> --collection <collection_name> 'path to source';

// csv
// backup
// mongoexport --db <db_name> --collection <col_name> --type='csv' --fileds 'comma seperated value(proeprty)' --out <desitnation folder with .csv extention'


// restore
// mongoimport --db <db_name> --collection <col_name> --type=csv <path_to_source file> --headerline