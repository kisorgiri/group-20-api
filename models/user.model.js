const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    // db modeling
    name: {
        type: String,
        lowercase: true,
        trim: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phoneNumber: Number,
    role: {
        type: Number, // 1 for admin, 2 for normal user
        default: 2
    },
    status: {
        type: String,
        enum: ['active', 'inactive'],
        default: 'active'
    },
    dob: Date,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    address: {
        type: String
    },
    email: {
        type: String,
        unique: true,
        sparse: true,
    },
    passwordResetToken: String,
    passwordResetTokenExpiry: Date
}, {
    timestamps: true
})

const UserModel = mongoose.model('user', userSchema);
module.exports = UserModel;