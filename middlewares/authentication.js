const jwt = require('jsonwebtoken');
const config = require('./../configs');
const UserModel = require('./../models/user.model');

module.exports = function (req, res, next) {
    var token;
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token']
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization']
    }
    if (req.headers['token']) {
        token = req.headers['token']
    }
    if (req.query.token) {
        token = req.query.token;
    }
    if (token) {
        // verification
        jwt.verify(token, config.JWT_secret, function (err, decoded) {
            if (err) {
                return next(err);
            }
            console.log('decoded value >>>', decoded);
            UserModel.findById(decoded.id)
                .exec(function (err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        req.loggedInUser = user;
                        return next();
                    } else {
                        next({
                            msg: 'User Removed from system'
                        })
                    }
                })
        })


    } else {
        next({
            msg: "Token Not Provided"
        })
    }

}
